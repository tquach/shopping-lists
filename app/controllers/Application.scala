package controllers

import play.api._
import play.api.mvc._

import models._

import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._

import play.api.Play.current

object Application extends Controller {

  def index = Action {
    Ok(views.html.index(List()))
  }

  def find(name: String) = DBAction { implicit request =>
  	val shoppingLists = Query(ShoppingLists).list
  	Ok(views.html.index(shoppingLists))
  }
}