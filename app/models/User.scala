package models

import scala.slick.driver.PostgresDriver._

case class User(id: Long,
    email: String)

object Users extends Table[User]("USERS") {
    def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)
    def email = column[String]("EMAIL")
    def * = id ~ email <> (User, User.unapply _)
}