package models

import scala.slick.driver.PostgresDriver._

case class ShoppingList(id: Long,
	name: String,
	user: Long)

object ShoppingLists extends Table[ShoppingList]("SHOPPINGLISTS") {
	def id = column[Long]("ID", O.PrimaryKey)
	def name = column[String]("NAME")
	def user = column[Long]("USER_ID")
	def * = id ~ name ~ user <>
		(ShoppingList, ShoppingList.unapply _)
}