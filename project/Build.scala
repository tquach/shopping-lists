import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "play22-slick-demo"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    "com.typesafe.slick" %% "slick" % "1.0.1",
    "org.slf4j" % "slf4j-nop" % "1.6.4",
    jdbc,
    "org.postgresql" % "postgresql" % "9.2-1002-jdbc4"
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    scalaVersion := "2.10.2"
    
  ).dependsOn(RootProject( uri("git://github.com/freekh/play-slick.git") ))
}